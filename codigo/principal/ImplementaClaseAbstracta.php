<?php

/**
 * Clase que implementa la clase abstracta
 * 
 * Los comentarios de la clase abstracta son herredados por la clase que la 
 * implementa a menos de que esta los sobre escriba.
 * @author AndresRuiz <andresruizids@gmail.com>
 * @package codigo
 * @category general
 * @since 19.1.31
 */

namespace codigo;

include('ClaseAbstracta.php');

/**
 * Clase que implemnta
 * @author AndresRuiz <andresruizids@gmail.com>
 */
class ImplementaClaseAbstracta implements ClaseAbstracta
{


    /**
     * una variable estatica
     * @static
     */
    public static $variableEstatica = 0;


    /**
     * Método implementado por la clase
     * 
     * @author Sergio <correo@correo.com>
     * @param int $variable variable que ingresa al metodo.
     */
    public function funcionAbstracataGuardar($variable)
    {
       # Implementación de la funcion abstracta.
        echo $variable;
    }

    /**
     * Esta es una funcion privada
     * @author AndresRuiz <andresruizids@gmail.com>
     * @access private
     */
    public function FuncionPrivada()
    {
        echo 'codigo de la funcion';
    }


    /**
     * Aca un ejemplo:
     * <?php
     * echo strlen('6');
     * ?>
     * @example codigo/principal/Ejemplo.php  1 6 Forma de usar la función
     * @param bool|int $numero numero a evaluar
     * @return int|string cualquiera de los dos
     * @author Victor <correo@corre.com>
     * @version 1.0.2
     * @todo verificar que los datos sean correctos
     * @source 70 3 nada
     * 
     */
    public function funcionEjemplo($numero)
    {
        /**@var integer */
        $vari = 2;


        return 0;
    }

    /**
     * Funcion final, no debe ser sobre escrita
     * @final 
     */
    public function funcionFinal()
    {
        # code...
    }

}
