<?php

/**
 * informacion general de la clase  abstracta de CRUD
 *
 * Define las funciones CRUD para ser implementadas en todos
 * los formularios del proyecto.
 * @package codigo
 * @category general
 * @author AndresRuiz <andresruizids@gmail.com>
 * @since 19.1.31
 
 */


namespace codigo;

/**
 * es una clase abstracta
 * @abstract
 */
abstract class ClaseAbstracta
{
   /**
    * Función guardar objeto en la base de datos.
    * @param string $variable variable para guardar en la base de datos
    * @abstract
    */
   abstract function funcionAbstracataGuardar($variable);


}
