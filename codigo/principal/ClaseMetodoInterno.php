<?php
/**
 * Esto es un ejempo del metodo interno
 * @author AndresRuiz <andresruizids@gmail.com>
 * @package codigo
 * @category general
 */

 namespace codigo;

/**
 * Esto es una clase con metodo interno
 * 
 * @method int datos() datos(int $int1, int $int2) multiplica dos enteros
 */
class ClaseMetodoInterno
{
    /**
     * @param int $metodo algun metodo
     * @param array $parametros Los parametros que entrar.
     */
    function __llamado($metodo, $parametros)
    {
        if ($metodos == 'datos') {
            if (count($parametros) == 2) {
                return $parametros[0] * $parametros[1];
            }
        }
    }
}
