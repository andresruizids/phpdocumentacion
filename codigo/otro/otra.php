<?php
/**
 * Clase para el uso de otras aplicaciones 
 * 
 * @author AndresRuiz
 * @since 19.1.31
 * @package otro
 * @category otraCategoria
 * @todo implentar todo lo demás acá
 */

namespace otro;

/**
 * Clase otra
 * 
 * Esta clase es solo otra clase más.
 */
class otra
{
//Esto es un comentario que no se documentara
    private $propiedadUnoPrivada;

    /**
     * @var int $propiedadDosPublica variable usada como ejemplo  
     * */
    public $propiedadDosPublica;
    /**
     * @author AndresRuiz <andresruizids@gmail.com>
     * @deprecated desde la version 1.0.0 del aplicativo
     * @todo Reemplazar apariciones o usar otra variable
     * @var string $propiedadTresProtegida Es una variable protegida
     */
    protected $propiedadTresProtegida;

    /**
     * Metodo que imprime una variable
     *
     * Se debe de enviar un strin para un mejor funcionamiento
     * @param int|string $variable dato a ser imprimido
     */
    public function funcionImprime($variable)
    {
        echo $variable;
    }

    /**
     * Metodo que imprime una variable
     *
     * Se debe de enviar un strin para un mejor funcionamiento
     * @param string $variable dato a ser imprimido
     */
    public function funcionImprimeString(string $variable)
    {
        echo $variable;
    }
}
 