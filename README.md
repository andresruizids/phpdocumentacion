# Ejemplo de docuemntación de código PHP con phpDocumenter

## Tags estandar
~~~
@access
@author
@copyright
@deprecated
@example
@ignore
@internal
@link
@see
@since
@tutorial
@version
~~~

## Tags para declaración de funciones

Puede contener los tags estandar además de los siguientes tags:
~~~
@global
@param
@return
@staticvar
~~~

## Elementos de clase

Puede contener los tags estandar además de los siguientes tags::
~~~
@abstract
@package
@subpackage
@static
~~~
[Instalacion de phpDocumentor](https://github.com/phpDocumentor/phpDocumentor2)

***
Para usar phpDocumentor desde composer se debe de correr el siguiente comando:
~~~
vendor/bin/phpdoc run -d dondeguardar -t quedocumentar --sourcecode
~~~
para generar graficas de forma adecuada es necesario tener instalado el generador    

      sudo apt-get install graphviz


Error en la vista de los source

     https://github.com/phpDocumentor/phpDocumentor2/issues/1998